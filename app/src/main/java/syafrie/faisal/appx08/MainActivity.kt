package syafrie.faisal.appx08

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener{
    override fun onClick(v: View?){
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        prefEditor.putString(FIELD_TEXT,edText.text.toString())
        prefEditor.putInt(FIELD_FONT_SIZE,sbar.progress)
        prefEditor.commit()
        Toast.makeText(this,"Perubahan telah disimpan", Toast.LENGTH_SHORT).show()
    }

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val FIELD_FONT_SIZE = "font_size"
    val FIELD_TEXT = "teks"
    val DEF_FONT_SIZE =12
    val DEF_TEXT = "Hello World"

    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress:Int,
                                       fromuser:Boolean){
            edText.setTextSize(progress.toFloat())
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?){

        }

        override fun onStopTrackingTouch(seekBAr: SeekBar?){

        }

    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        edText.setText(preferences.getString(FIELD_TEXT,DEF_TEXT))
        edText.textSize = preferences.getInt(FIELD_FONT_SIZE, DEF_FONT_SIZE).toFloat()
        sbar.progress = preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE)
        sbar.setOnSeekBarChangeListener(onSeek)
        btnSimpan.setOnClickListener(this)
    }
}